# RabbitMQ template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[RabbitMQ](https://www.rabbitmq.com/) database inside of a
[FreeBSD](https://www.freebsd.org/) jail.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-rabbitmq.git
```

## Usage

```
# bastille template TARGET wegtam/bastille-rabbitmq
```

